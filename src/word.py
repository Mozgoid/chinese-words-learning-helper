# -*- coding: utf-8 -*-

import codecs


class Word(object):
    def __init__(self, pinyin, hieroglyph, translition):
        self.pinyin = pinyin
        self.hieroglyph = hieroglyph
        self.translition = translition

    def __unicode__(self):
        return u'{} - {} - {}'.format(
            self.pinyin, self.hieroglyph, self.translition)


def read_from_txt(filename):
    words = []
    with codecs.open(filename, encoding='utf-8') as f:
        for line in f.readlines():
            line = line.replace(u'‐', u'-')
            splited = line.split(u'-')
            with_hieroglyph = len(splited) == 3
            pinyin = splited[0].strip()
            hieroglyph = (splited[1] if with_hieroglyph else u'').strip()
            ru = splited[2 if with_hieroglyph else 1].strip()
            words.append(Word(pinyin, hieroglyph, ru))
    return words

