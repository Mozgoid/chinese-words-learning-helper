# -*- coding: utf-8 -*-
import sys


def play(filename):
    pass


def show_notification(title, msg):
    pass


if sys.platform in ("linux", "linux2"):
    import subprocess

    def linux_play(filename):
        subprocess.Popen(('mpg123', filename),
                         stdout=subprocess.PIPE,
                         stderr=subprocess.PIPE)

    def linux_show_notification(title, msg):
        subprocess.Popen(['notify-send', title, msg])

    play = linux_play
    show_notification = linux_show_notification


elif sys.platform == "win32":
    from win32api import *
    from win32gui import *
    import win32con
    import sys
    import os

    class WindowsBalloonTip:
        def __init__(self, title, msg):
            message_map = {
                win32con.WM_DESTROY: self.OnDestroy,
            }
            # Register the Window class.
            wc = WNDCLASS()
            hinst = wc.hInstance = GetModuleHandle(None)
            wc.lpszClassName = "PythonTaskbar"
            wc.lpfnWndProc = message_map  # could also specify a wndproc.
            classAtom = RegisterClass(wc)
            # Create the Window.
            style = win32con.WS_OVERLAPPED | win32con.WS_SYSMENU
            self.hwnd = CreateWindow(classAtom, "Taskbar", style, 0, 0,
                                     win32con.CW_USEDEFAULT,
                                     win32con.CW_USEDEFAULT,
                                     0, 0, hinst, None)
            UpdateWindow(self.hwnd)
            iconPathName = os.path.join(sys.path[0], "balloontip.ico")
            iconPathName = os.path.abspath(iconPathName)
            icon_flags = win32con.LR_LOADFROMFILE | win32con.LR_DEFAULTSIZE
            try:
                hicon = LoadImage(hinst, iconPathName,
                                  win32con.IMAGE_ICON, 0, 0, icon_flags)
            except:
                hicon = LoadIcon(0, win32con.IDI_APPLICATION)
            flags = NIF_ICON | NIF_MESSAGE | NIF_TIP
            nid = (self.hwnd, 0, flags, win32con.WM_USER+20, hicon, "tooltip")
            Shell_NotifyIcon(NIM_ADD, nid)
            Shell_NotifyIcon(NIM_MODIFY, (self.hwnd, 0,
                                          SNIF_INFO, win32con.WM_USER+20,
                             hicon, "Balloon  tooltip", msg, 200, title))
            # self.show_balloon(title, msg)
            time.sleep(10)
            DestroyWindow(self.hwnd)

        def OnDestroy(self, hwnd, msg, wparam, lparam):
            nid = (self.hwnd, 0)
            Shell_NotifyIcon(NIM_DELETE, nid)
            PostQuitMessage(0)  # Terminate the app.

    def win_show_notification(title, msg):
        WindowsBalloonTip(title, msg)

    show_notification = win_show_notification
