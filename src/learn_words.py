# -*- coding: utf-8 -*-

import time
import os
import sys
import signal

import db
import platform_dependant
import word


sounddir = os.path.join(os.path.dirname(os.path.abspath(__file__)), '../')
beepsound = os.path.join(sounddir, 'beep.mp3')


class LearnTool(object):
    progression = [2, 3, 20]

    def __init__(self, words, n=0):
        self.n = n
        self.words = words
        self.wordsinprogress = []

    def start(self, threadcount=3, delay=4  ):
        self.add_word(0.18)
        self.wordsinprogress.append([None, [delay]])
        self.wordsinprogress.append([None, [delay*2]])
        while self.learnnext():
            pass

    def add_word(self, delay):
        if not self.words:
            return
        word, self.words = self.words[0], self.words[1:]
        times = [delay] + self.progression
        self.wordsinprogress.append([word, times])
        self.n = self.n + 1
        print self.n
        platform_dependant.show_notification(word.pinyin, unicode(word))

    def get_input(self):
        TIMEOUT = 60

        def interrupted(signum, frame):
            platform_dependant.play(beepsound)

        while True:
            signal.signal(signal.SIGALRM, interrupted)
            signal.alarm(TIMEOUT)
            try:
                inp = raw_input().decode(sys.stdin.encoding)
                signal.alarm(0)
                return inp
            except:
                pass

    def check(self, word):
        if not word:
            return

        done = False
        print 'please translate:', word.translition + u':'

        while not done:
            inp = self.get_input()
            if inp == 'help':
                platform_dependant.show_notification(word.pinyin, word.hieroglyph)
                time.sleep(30)
            else:
                done = inp == word.hieroglyph
                if not done:
                    print 'no'

    def learnnext(self):
        self.wordsinprogress.sort(key=lambda w: w[1][0])

        current_word = self.wordsinprogress[0]
        minslip = current_word[1][0]
        time.sleep(minslip * 60)
        for w in self.wordsinprogress:
            w[1][0] = w[1][0] - minslip

        platform_dependant.play(beepsound)
        self.check(current_word[0])

        if len(current_word[1]) > 1:
            current_word[1] = current_word[1][1:]
        else:
            print 'learned:', unicode(current_word[0])
            self.wordsinprogress = self.wordsinprogress[1:]
            self.add_word(0)

        return self.wordsinprogress


if __name__ == '__main__':
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument('-w', '--wordsfile',
                        help='file with words')
    parser.add_argument('-b', '--begin', type=int,
                        help='range', default=0)
    parser.add_argument('-e', '--end', type=int,
                        help='range')
    args = parser.parse_args()
    args = parser.parse_args()
    words = []
    n = 0
    if args.wordsfile:
        words = word.read_from_txt(args.wordsfile)
    if args.end:
        n = args.begin
        d = db.DB(db.DBNAME)
        words = d.get_all_words()[args.begin:args.end]
        d.conn.close()

    pn = LearnTool(words)
    pn.start()
