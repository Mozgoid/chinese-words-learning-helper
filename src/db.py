# -*- coding: utf-8 -*-
import sqlite3
import os

import word

dbdir = os.path.join(os.path.dirname(os.path.abspath(__file__)), '../words/')
DBNAME = os.path.join(dbdir, 'words.db')
XLSPATH = os.path.join(dbdir, '10000_popular_words.xls')


class DB(object):
    def __init__(self, dbname):
        self.conn = sqlite3.connect(dbname)

    def createdb(self):
        self.conn.execute('''CREATE TABLE words
            (
                id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
                hieroglyph TEXT NOT NULL,
                pinyin TEXT NOT NULL,
                translition TEXT NOT NULL
            );''')
        self.conn.commit()

    def populate(self, xlspath):
        from pyexcel_xls import get_data
        data = get_data(xlspath)[1:]
        words = [(row[3], row[5], row[6]) for row in data]

        self.conn.executemany('''
            INSERT INTO words
            (hieroglyph, pinyin, translition)
            VALUES (?,?,?)''', words)
        self.conn.commit()

    def get_all_words(self):
        all = []
        query = '''SELECT * FROM words'''
        for row in self.conn.execute(query):
            all.append(word.Word(row[2], row[1], row[3]))
        return all

if __name__ == '__main__':
    db = DB(DBNAME)
    all = db.get_all_words()
    db.conn.close()
